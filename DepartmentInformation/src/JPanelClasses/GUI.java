package JPanelClasses;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.awt.FlowLayout;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.util.stream.Collectors;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class GUI extends JFrame {	
	
	JFrame frame;
	JPanel panel1;
	ImageIcon logo;
	JLabel img, lblGender, lblTotal;
	JComboBox courseBox;
	ButtonGroup grpGender;
	JRadioButton radFemale, radMale;
	JScrollPane scroll;
	JTable table;
	String gender;
	String courseName;
	String course;
	
	//Constructor
	GUI(StudentShowInfo info)
	{
		frame = new JFrame("College of Science and Computer Studies");
		
		panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		
		logo = new ImageIcon("C:/Users/bootcamp/Documents/departmentactivity/DepartmentInformation/src/JPanelClasses/logo.png");
		img = new JLabel(logo);
		img.setBounds(100,-40,200,200);
		
		//COMBO BOX

		String courses[] = {"---", "BS Information Technology", "BS Computer Science", "BS Human Biology", "BS Environmental Science", "BS Applied Mathematics"};
		
		courseBox = new JComboBox(courses);
		courseBox.setBounds(50,120,300,20);
		
		//GENDER
		lblGender = new JLabel("Gender");
		lblGender.setBounds(50,160,100,20);
		
			//RADIO BUTTONS
		grpGender = new ButtonGroup();
		radFemale = new JRadioButton("Female");
		radMale = new JRadioButton("Male");
		radFemale.setBounds(70,190,100,20);
		radMale.setBounds(70,210,100,20);
		
			//BUTTON GROUP
		grpGender.add(radFemale);
		grpGender.add(radMale);
		
		table = new JTable();
		//ITEM LISTENER (COMBO BOX)
		
		courseBox.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int select = e.getStateChange();
				if (select == ItemEvent.SELECTED)
				{
					System.out.println(e.getItem());
					courseName = String.valueOf(courseBox.getSelectedItem());
					if(courseName.equals("BS Information Technology")){
						course = "BSIT";
					}else if(courseName.equals("BS Computer Science")){
						course = "BSCS";
					}else if(courseName.equals("BS Human Biology")){
						course = "BSBIO";
					}else if(courseName.equals("BS Environmental Science")){
						course = "BSENV";
					}else if(courseName.equals("BS Applied Mathematics")){
						course = "BSAPM";
					}
				}
			}
		});
		
		//TOTAL

		lblTotal = new JLabel();
		lblTotal.setBounds(50,250,50,50);
		
		if (radFemale.isSelected()) {
			lblTotal.setText("Total Female: ");
			frame.add(lblTotal);
			setVisible(true);
			
		}
		if (radMale.isSelected()){
			lblTotal.setText("Total Male: ");
			frame.add(lblTotal);
			setVisible(true);
			
		}
		
		//TABLE
		
			//HEADER
		//String[] header = {"First Name", "Middle Name", "Last Name"};
		
		
		/*EUNICE
        textArea = new JTextArea();
        textArea.setEditable(false);
        textArea.setBounds(50,250,300,170);
        scroll = new JScrollPane(textArea);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);*/
        
        	//ACTION

		lblTotal = new JLabel("Total: ");
		lblTotal.setBounds(60,210,100,100);
		
			//ITEM LISTENER (RADIO BUTTONS)
		
		radFemale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int state = e.getStateChange();
				if (state == ItemEvent.SELECTED) 
				{
					try {
						DefaultTableModel infoModel = info.load(course, "F"); 
						table.setModel(infoModel);
						lblTotal.setText("Total Female: " + infoModel.getRowCount());
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		radMale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int state = e.getStateChange();
				if (state == ItemEvent.SELECTED) 
				{
					try {
						DefaultTableModel infoModel = info.load(course, "M"); 
						table.setModel(infoModel);
						lblTotal.setText("Total Male: " + infoModel.getRowCount());
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		
		//ACTION
        /*ActionListener comboAction = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e)
			{
				String c = (String) courseBox.getSelectedItem();
				
				}
			}
		};
		
		courseBox.addActionListener(comboAction);*/
		

		//TABLE
        /*Object[][] data= { 
                {
                    "John","Aeriel","So"
                }        
        };*/

        //HEADER
        //String[] header = {"First Name", "Middle Name", "Last Name"};
        
        //TABLE
       // table = new JTable(data,header);
        scroll = new JScrollPane (table);
        scroll.setBounds(35, 280, 360, 210);
        table.setEnabled(false);
		
        
		frame.add(panel1);
		frame.add(img);
		frame.add(courseBox);
		frame.add(lblGender);
		frame.add(radFemale);
		frame.add(radMale);
		frame.add(lblTotal);
		frame.add(scroll);
		//frame.add(textArea);
		frame.add(scroll);  
		frame.setLayout(null);
		frame.setSize(420,500); //size of the GUI
		frame.setLocationRelativeTo(null); //to center the GUI on the screen
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //exits the app when "x" is clicked
        frame.setVisible(true); //allows the window to appear
	}
			
	public static void main(String args[]) {
		StudentShowInfo info = new StudentShowInfo();
		new GUI(info);
	}
	

}
