package JPanelClasses;
//import StudentData.dbTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class StudentShowInfo  {
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/studentinfo";
	   private static Connection con;

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "";
	   
	   public static int total;
	   
	   public static Connection getDBConnection() {
		   try {
	           Class.forName(JDBC_DRIVER);
	           try {
	               con = DriverManager.getConnection(DB_URL, USER, PASS);
	             
	           } catch (SQLException ex) {
	               // log an exception. fro example:
	               System.out.println("Failed to create the database connection."); 
	           }
	       } catch (ClassNotFoundException ex) {
	           // log an exception. for example:
	           System.out.println("Driver not found."); 
	       }
	       return con;
	   }
	   
	public ArrayList<Student> getAllStudent(String course, String gender) throws ClassNotFoundException, SQLException{
		try {
	           Class.forName(JDBC_DRIVER);
	           try {
	               con = DriverManager.getConnection(DB_URL, USER, PASS);
	             
	           } catch (SQLException ex) {
	               // log an exception. fro example:
	               System.out.println("Failed to create the database connection."); 
	           }
	       } catch (ClassNotFoundException ex) {
	           // log an exception. for example:
	           System.out.println("Driver not found."); 
	       }
		
		Statement stm;
	    stm = con.createStatement();
	    String sql = "Select * From students WHERE Course = '" + course + "' AND Gender = '" + gender + "'";
	    PreparedStatement pst = con.prepareStatement(sql);
	    ResultSet rst;
	    rst = pst.executeQuery();
	    ArrayList<Student> customerList = new ArrayList<Student>();
	    while (rst.next()) {
	        Student customer = new Student(rst.getString("FirstName").toString(), rst.getString("MiddleName").toString(), rst.getString("LastName").toString(), rst.getString("Gender").toString());
	        customerList.add(customer);
	        System.out.println(customer.getFName());
	    }
	    return customerList;
	}
	
	public DefaultTableModel load(String courseName, String gender) throws ClassNotFoundException, SQLException {
		Object columnNames[] = { "First Name", "Middle Initial", "Last Name", "Gender" };
		ArrayList<Student> studentList = getAllStudent(courseName, gender);
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		int total = studentList.size();
		
		System.out.println(studentList.size());
		for (int i = 0; i < studentList.size(); i++){
			   String firstName = studentList.get(i).getFName();
			   String middleName = studentList.get(i).getMInitial();
			   String lastName = studentList.get(i).getLName();
			   String genderData = studentList.get(i).getGender();
			   
			   Object[] data = {firstName, middleName, lastName, genderData};
			   
			   System.out.println(firstName);
			   model.addRow(data);
			   
			   System.out.println(firstName + " " + middleName);

			}
		
		return model;
}
	
	
	
	
}
