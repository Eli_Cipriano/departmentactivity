package JPanelClasses;


//implementing Type casting, Inheritance, & Encapsulation. Student is the parent class, Person is child
public class Student {
	private String fName;
	private String mInitial;
	private String lName;
	private String gender;
	
	
	public Student(String fName, String mInitial, String lName, String gender){
		this.fName = fName;
		this.mInitial = mInitial;
		this.lName = lName;
		this.gender = gender;
	}
	
	//getters
	public String getFName(){
		return fName;
	}
	
	public String getMInitial() {
		return mInitial;
	}
	
	public String getLName() {
		return lName;
	}
	
	public String getGender() {
		return gender;
	}
	
	//setters
	public void setFName(String newFName){
		this.fName = newFName;
	}
	
	public void setMName(String newMName){
		this.mInitial = newMName; //type cast from string to char
	}
	
	public void setLName(String newLName){
		this.lName = newLName;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	

}